#ARG docker_base_image_url
#FROM $docker_base_image_url
FROM python:3.11

RUN apt update -y && apt install curl --no-install-recommends -y
RUN apt install -y npm
RUN npm init playwright@latest

COPY requirements.txt requirements.txt
COPY scrapy.cfg scrapy.cfg
COPY setup.py setup.py
RUN pip install -r requirements.txt
RUN playwright install --with-deps

COPY app /app
WORKDIR /

EXPOSE 7861

ENTRYPOINT ["python3", "/app/app.py"]
