#!/usr/bin/env bash
set -eo pipefail

python --version
python -m venv venv
source ./venv/bin/activate
pip install --upgrade pip
pip install -r requirements-dev.txt
