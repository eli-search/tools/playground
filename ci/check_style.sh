#!/usr/bin/env bash
set -euo pipefail

source ./venv/bin/activate
pycodestyle "app/" tests
