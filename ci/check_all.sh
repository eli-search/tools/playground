#!/usr/bin/env bash
set -euo pipefail

bash ci/check_style.sh
bash ci/check_typing.sh
bash ci/check_dependencies.sh
bash ci/check_security_issues.sh
bash ci/check_code_smells.sh
