import json
import logging

from typing import TypeVar, Type


ModelTypeVar = TypeVar("ModelTypeVar")
logger = logging.getLogger(__name__)


def filter_none_in_dict(dict_: dict) -> dict:
    return {k: v for k, v in dict_.items() if v is not None}


def load_json(
    parsed_class: Type[ModelTypeVar], data_raw: str, url: str
) -> ModelTypeVar:
    try:
        data_dict = json.loads(data_raw, object_hook=filter_none_in_dict)
        if isinstance(data_dict, dict):
            return parsed_class(**data_dict)
    except Exception as err:
        # TODO: create dedicated exception?
        print(
            f"Fail to parse JSON string into {parsed_class} (url: {url}, error: {err})"
        )
    return parsed_class()
