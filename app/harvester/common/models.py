from dataclasses import dataclass
from enum import Enum
from typing import List, Optional


@dataclass
class Item:
    uri: str
    last_mod_date: str
    raw_data: bytes
    date_applicability: Optional[str] = None
    date_document: Optional[str] = None
    date_no_longer_in_force: Optional[str] = None
    date_publication: Optional[str] = None
    embodies: Optional[List[str]] = None
    first_date_entry_in_force: Optional[str] = None
    id_local: Optional[str] = None
    in_force: Optional[List[str]] = None
    is_about: Optional[List[str]] = None
    is_embodied_by: Optional[List[str]] = None
    is_exemplified_by: Optional[List[str]] = None
    is_realized_by: Optional[List[str]] = None
    language: Optional[str] = None
    passed_by: Optional[List[str]] = None
    published_in: Optional[List[str]] = None
    realizes: Optional[List[str]] = None
    title: Optional[str] = None
    title_alternative: Optional[str] = None
    title_short: Optional[str] = None
    transposes: Optional[List[str]] = None
    type_document: Optional[str] = None
    work_type: Optional[List[str]] = None

    def __repr__(self):
        """only print out uri after exiting the Pipeline"""
        simplified_content = repr({"uri": self.uri})
        return f"Item({simplified_content})"


class ScrapingSource(Enum):
    ATOM_FEED = "ATOM_FEED"
    SITEMAP = "SITEMAP"


class QueryType(Enum):
    RDF = "RDF"
    FTS = "FTS"


class JobStatus(Enum):
    SUCCESS = "SUCCESS"
    FAIL = "FAIL"
