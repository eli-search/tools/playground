import logging

from harvester.common.models import ScrapingSource
from db.db_client import DBClient

logger = logging.getLogger(__name__)


class MarkDeletedResources:
    def close_spider(self, spider):
        harvester_config = spider.settings["HARVESTER_CONFIG"]
        db_config = harvester_config.db_config
        if harvester_config.scraping_source is ScrapingSource.SITEMAP:
            if spider.scraped_ids:
                job_id = str(harvester_config.job_config.job_id)
                DBClient(db_config).mark_as_deleted(job_id, spider.scraped_ids)
            else:
                logger.warning(
                    f"Nothing scraped by the scraper: {harvester_config.job_config.spider_name}"
                )
