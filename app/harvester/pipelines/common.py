import logging
from datetime import datetime
from pathlib import Path
from typing import AnyStr

from harvester.common.models import QueryType
from harvester.harverser_config import HarvesterConfig
from db.db_client import DBClient
from itemadapter import ItemAdapter
from rdflib import Graph, URIRef, Literal
from rdflib.plugins.sparql import prepareQuery


EXCLUDED_ATTRIBUTES = ["uri", "raw_data", "last_mod_date"]
logger = logging.getLogger(__name__)


def create_node(subject: URIRef, predicate: URIRef, value: str) -> tuple:
    if value.startswith("https:") or value.startswith("http:"):
        return subject, predicate, URIRef(value)
    return subject, predicate, Literal(value)


def generate_graph(item_adapter: ItemAdapter) -> Graph:
    uri = item_adapter["uri"]
    logger.debug(f"Generating graph for resource: {uri}")
    g = Graph()
    subject = URIRef(item_adapter["uri"])
    for attribute in item_adapter.keys():
        if attribute not in EXCLUDED_ATTRIBUTES:
            predicate = URIRef(f"http://data.europa.eu/eli/ontology#{attribute}")
            content = item_adapter[attribute]
            if content:
                if isinstance(content, list):
                    for element in content:
                        g.add(create_node(subject, predicate, element))
                else:
                    g.add(create_node(subject, predicate, content))
    logger.debug(f"Graph for resource: {uri} successfully generated")
    return g


def apply_enrichment_and_filtering_queries(
    resource_uri: str,
    harvester_config: HarvesterConfig,
    g: Graph,
    query_type: QueryType,
) -> Graph:
    queries = DBClient(harvester_config.db_config).get_filtering_queries(query_type)
    logger.debug("Applying enrichment and filtering queries")
    result_graph = Graph()
    for query_str in queries:
        if "$URL" in query_str:
            query_str = query_str.replace("$URL", resource_uri)
        query = prepareQuery(query_str)
        query_result = g.query(query)
        for result in query_result:
            if isinstance(result, tuple) and len(result) == 3:
                result_graph.add(result)
    logger.debug("Enrichment and filtering queries applied successfully")
    return result_graph


def transform_uri(resource_uri: str) -> str:
    if "http://" in resource_uri:
        resource_uri = resource_uri.replace("http://", "")
    if "https://" in resource_uri:
        resource_uri = resource_uri.replace("https://", "")
    if "www." in resource_uri:
        resource_uri = resource_uri.replace("www.", "")
    transformed_uri = resource_uri.replace("/", "_").replace(".", "_").replace(":", "_")
    return transformed_uri


def generate_filename(config: HarvesterConfig, extension: str):
    date = datetime.now().strftime("%Y%m%d")
    filename = f"{date}_{config.job_config.job_id}_{config.execution_id}.{extension}"
    return filename


def store_result_files(
    harvester_config: HarvesterConfig,
    uri: str,
    last_mod_date: str,
    content: AnyStr,
    file_extension: str,
    input_bucket: bool,
    working_bucket: bool,
) -> str:
    logger.debug(f"Storing {file_extension} files for resource: {uri}")
    job_config = harvester_config.job_config
    writer = harvester_config.writer

    filename = generate_filename(harvester_config, file_extension)
    transformed_uri = transform_uri(uri)
    path = Path(job_config.output_path) / Path(transformed_uri) / Path(last_mod_date)
    if harvester_config.local_process:
        if input_bucket:
            filename = f"raw_{filename}"
        logger.debug(f"Writing {filename} to {path}")
        writer.write(str(path), filename, content)
    else:
        if input_bucket:
            output_filename = f"{job_config.spider_name}/{str(path)}/{filename}"
            logger.debug(f"Writing {job_config.output_bucket}/{output_filename}")
            writer.write(job_config.output_bucket, output_filename, content)
        if working_bucket:
            working_filename = f"{harvester_config.execution_id}/{file_extension}/{transformed_uri}.{file_extension}"
            logger.debug(
                f"Writing {job_config.output_working_bucket}/{working_filename}"
            )
            writer.write(job_config.output_working_bucket, working_filename, content)
    logger.debug(f"Files for resource: {uri} stored successfully")
    return f"{str(path)}/{filename}"
