import logging

from scrapy.crawler import CrawlerProcess
from scrapy import spiderloader
from scrapy.utils import project

from harvester.common.exceptions import SpiderException
from harvester.harverser_config import HarvesterConfig

logger = logging.getLogger(__name__)


class Harvester:
    def __init__(self, input_data: dict):
        self.input_data = input_data
        self.harvester_config = HarvesterConfig(self.input_data)

    def harvest(self):
        logger.info(f"Start harvesting with input data: {self.input_data}")
        config = HarvesterConfig(self.input_data)
        urls_to_scrape = config.url_parser.get_urls()
        settings = project.get_project_settings()
        settings.set("HARVESTER_CONFIG", config)
        spider_loader = spiderloader.SpiderLoader.from_settings(settings)
        available_spiders = spider_loader.list()
        if config.job_config.spider_name in available_spiders:
            logger.info(f"Loading spider {config.job_config.spider_name}")
            spider = spider_loader.load(config.job_config.spider_name)
            logger.info("Start crawling")
            process = CrawlerProcess(settings=settings, install_root_handler=False)
            process.crawl(spider, urls=urls_to_scrape)
            process.start()
        else:
            raise SpiderException(
                f"Spider '{config.job_config.spider_name}' does not exist, available spiders : {available_spiders}"
            )
