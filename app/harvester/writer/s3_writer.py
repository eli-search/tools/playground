import logging
from typing import AnyStr

from aws.s3_client import S3Client
from harvester.writer.file_writer import FileWriter

logger = logging.getLogger(__name__)


class S3Writer(FileWriter):
    def write(self, directory: str, filename: str, content: AnyStr) -> None:
        logger.debug(f"Writing file on S3 with bucket: {directory} and key: {filename}")
        S3Client().write_file_to_s3(content, directory, filename)
