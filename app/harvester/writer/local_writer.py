import logging
import os
from pathlib import Path

from typing import TypeVar
from harvester.writer.file_writer import FileWriter

logger = logging.getLogger(__name__)
T = TypeVar("T")


class LocalWriter(FileWriter):
    def write(self, directory: str, filename: str, content: bytes) -> None:
        path = Path(directory)
        logger.info(
            f"Writing file on local system with path: {path} and filename: {filename}"
        )
        if not path.exists():
            os.makedirs(path)
        with open(path / filename, "wb") as file:
            file.write(content)
