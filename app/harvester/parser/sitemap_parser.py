import logging
from datetime import datetime

from harvester.parser.url_parser import UrlParser

logger = logging.getLogger(__name__)


def get_child_sitemaps(xml):
    sitemaps = xml.find_all("sitemap")
    output = []
    for sitemap in sitemaps:
        output.append(sitemap.findNext("loc").text)
    return output


def get_eli_urls(xml) -> list[dict[str, str]]:
    urls = xml.find_all("url")
    output = []
    for url in urls:
        url_value = url.findNext("loc").text
        last_mod_date = datetime.fromisoformat(url.findNext("lastmod").text)
        if "eli" in url_value:
            output.append(
                {
                    "url": url_value,
                    "last_mod_date": last_mod_date.strftime("%Y-%m-%dT%H-%M-%S"),
                }
            )

    return output


class SitemapParser(UrlParser):
    def get_urls(self) -> list[dict[str, str]]:
        logger.info(f"Retrieving URLs from Sitemap: {self.url}")
        output = []
        xml = super().get_xml_content()
        child_sitemaps = get_child_sitemaps(xml)
        if child_sitemaps:
            for sitemap_url in child_sitemaps:
                logger.debug(f"Parsing child sitemap {sitemap_url}")
                xml = UrlParser(sitemap_url).get_xml_content()
                output.extend(get_eli_urls(xml))
        else:
            xml = UrlParser(self.url).get_xml_content()
            output.extend(get_eli_urls(xml))
        logger.info(f"{len(output)} URLs retrieved")
        return output
