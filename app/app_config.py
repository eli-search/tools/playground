import logging
import os

from harvester.common.exceptions import GetConfigException
from db.db_client import DBClient
from db.db_config import DBConfig
from db.db_models import JobConfig

logger = logging.getLogger(__name__)


def is_local():
    return "ELI_SECRETSMANAGER_CREDS" not in os.environ


class AppConfig:
    def __init__(self, input_data: dict):
        logger.info("Start loading the application configuration")
        self.input_data = input_data
        self.execution_id = input_data["execution_id"]
        self.local_process = is_local()
        self.db_config = DBConfig(self.local_process)
        self.job_config = self.get_job_config()
        logger.info("Application configuration loaded successfully")

    input_data: dict
    execution_id: str
    local_process: bool
    db_config: DBConfig
    job_config: JobConfig

    def get_job_config(self) -> JobConfig:
        logger.info("Start loading the Job configuration configuration")
        try:
            db_client = DBClient(self.db_config)
            job_id = self.input_data["job_id"]
            job_config = db_client.get_by_primary_key(JobConfig, "job_id", job_id, None)
            logger.info("Job configuration successfully loaded")
            return job_config
        except Exception as err:
            raise GetConfigException(
                f"Error reading Job config from DB {self.db_config.host}:{self.db_config.port}/{self.db_config.dbname}:"
            ) from err
