import logging
import argparse
import uuid

from datetime import datetime, timedelta

from app_config import AppConfig
from db.db_client import DBClient
from db.db_models import ExecutionStatus
from harvester.common.models import JobStatus
from harvester.harvester_app import Harvester
from indexer.indexer_app import Indexer

LOG_LEVEL = logging.INFO

# Input
parser = argparse.ArgumentParser()
parser.add_argument("--job_id", type=str, help="Job ID")
parser.add_argument(
    "--scraping_source", type=str, help="Scraping source can be ATOM_FEED or SITEMAP"
)
parser.add_argument(
    "--last_scraping_date",
    type=str,
    help="Last time the ATOM_FEED scraping was executed. Expected format: YYYY-MM-DD HH-MM-SS",
    default=(datetime.now() - timedelta(days=2)).strftime("%Y-%m-%d %H:%M:%S"),
)
parser.add_argument(
    "--enable_indexing",
    action="store_true",
    help="Indexing phase is enable when this argument is present, disabled instead",
)
parser.add_argument(
    "--debug", action="store_true", help="Whether the debug mode is enabled"
)
args = parser.parse_args()
input_data = {
    "execution_id": str(uuid.uuid4()).replace("-", ""),
    "job_id": args.job_id,
    "scraping_source": args.scraping_source,
    "last_scraping_date": datetime.strptime(
        args.last_scraping_date, "%Y-%m-%d %H:%M:%S"
    ),
    "enable_indexing": args.enable_indexing,
    "debug": args.debug,
}

# Logging
if args.debug:
    LOG_LEVEL = logging.DEBUG

logging.basicConfig(
    level=LOG_LEVEL,
    format="%(asctime)s [%(name)s] %(levelname)s: %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)
logging.getLogger("scrapy").propagate = False
logger = logging.getLogger(__name__)


app_config = AppConfig(input_data)
try:
    # Execution status start
    execution_data = ExecutionStatus(
        execution_id=input_data["execution_id"],
        job_id=input_data["job_id"],
        start_date=datetime.now(),
    )
    DBClient(app_config.db_config).update_or_create(execution_data, "execution_id")

    # Harvesting
    harvester = Harvester(input_data)
    harvester.harvest()

    # Indexing
    indexer = Indexer(input_data)
    indexer.index()

    # Execution status end
    execution_data = ExecutionStatus(
        execution_id=input_data["execution_id"],
        job_id=input_data["job_id"],
        end_date=datetime.now(),
        status=JobStatus.SUCCESS,
    )
    DBClient(app_config.db_config).update_or_create(execution_data, "execution_id")
except Exception as err:
    error_message = f"{type(err).__name__}: {err}"
    execution_data = ExecutionStatus(
        execution_id=input_data["execution_id"],
        job_id=input_data["job_id"],
        end_date=datetime.now(),
        status=JobStatus.FAIL,
        error_msg=error_message,
    )
    DBClient(app_config.db_config).update_or_create(execution_data, "execution_id")
    logger.error(f"Error during execution of the app: {error_message}")
