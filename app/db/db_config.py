import json
import logging
import os

from aws.secrets_manager_client import SecretsManagerClient

logger = logging.getLogger(__name__)


class DBConfig:
    def __init__(self, local_process):
        logger.info("Start loading the Database configuration")
        if local_process:
            self.engine = os.environ["ELI_DB_ENGINE"]
            self.host = os.environ["ELI_DB_HOST"]
            self.username = os.environ["ELI_DB_USER"]
            self.password = os.environ["ELI_DB_PASSWORD"]
            self.dbname = os.environ["ELI_DB_DBNAME"]
            self.port = os.environ["ELI_DB_PORT"]
        else:
            secret = SecretsManagerClient().get_secret(
                os.environ["ELI_SECRETSMANAGER_CREDS"]
            )
            content = json.loads(secret["SecretString"])
            self.engine = content["engine"]
            self.host = content["host"]
            self.username = content["username"]
            self.password = content["password"]
            self.dbname = content["dbInstanceIdentifier"]
            self.port = content["port"]
        logger.info("Database configuration loaded successfully")

    engine: str
    host: str
    username: str
    password: str
    dbname: str
    port: str
