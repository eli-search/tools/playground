from datetime import datetime
from typing import Any

from sqlalchemy import ForeignKey
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from harvester.common.models import QueryType, JobStatus


class Base(DeclarativeBase):
    pass


class JobConfig(Base):
    __tablename__ = "job_config"
    job_id: Mapped[str] = mapped_column(primary_key=True)
    spider_name: Mapped[str] = mapped_column(default=None)
    sitemap_url: Mapped[str] = mapped_column(default=None)
    atom_url: Mapped[str] = mapped_column(default=None)
    output_path: Mapped[str] = mapped_column(default=None)
    output_bucket: Mapped[str] = mapped_column(default=None)
    output_working_bucket: Mapped[str] = mapped_column(default=None)


class ResourceData(Base):
    def __init__(self, **kw: Any):
        super().__init__(**kw)
        self.last_update = datetime.now()
        self.deleted = False

    __tablename__ = "resource_data"
    eli_uri: Mapped[str] = mapped_column(primary_key=True)
    job_id: Mapped[str] = mapped_column(ForeignKey("job_config.job_id"))
    metadata_path: Mapped[str] = mapped_column(default=None)
    metadata_timestamp: Mapped[datetime] = mapped_column(default=datetime.now())
    raw_data_path: Mapped[str] = mapped_column(default=None)
    raw_data_timestamp: Mapped[datetime] = mapped_column(default=datetime.now())
    last_update: Mapped[datetime] = mapped_column(default=datetime.now())
    last_indexation: Mapped[datetime] = mapped_column(default=None)
    deleted: Mapped[bool] = mapped_column(default=False)


class ExecutionStatus(Base):
    __tablename__ = "execution_status"
    execution_id: Mapped[str] = mapped_column(primary_key=True)
    job_id: Mapped[str] = mapped_column(ForeignKey("job_config.job_id"))
    start_date: Mapped[datetime] = mapped_column(default=None)
    end_date: Mapped[datetime] = mapped_column(default=None)
    status: Mapped[JobStatus] = mapped_column(default=None)
    error_msg: Mapped[str] = mapped_column(default=None)


class FilteringQuery(Base):
    __tablename__ = "filtering_queries"
    id: Mapped[str] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(default=None)
    query_type: Mapped[QueryType] = mapped_column(default=None)
    content: Mapped[str] = mapped_column(default=None)
    enabled: Mapped[bool] = mapped_column(default=True)
