import logging

import boto3

from harvester.common.exceptions import GetSSMException

logger = logging.getLogger(__name__)


class SSMClient:
    def __init__(self, region_name="eu-west-1"):
        self.client = boto3.client("ssm", region_name=region_name)

    def get_ssm_parameter(self, path: str) -> str:
        try:
            logger.debug(f"Retrieving parameter: '{path}' from SSM")
            parameter = self.client.get_parameter(Name=path)
            table_name = parameter["Parameter"]["Value"]
            logger.debug(f"Parameter: '{path}' retrieved from SSM")
            return table_name
        except Exception as err:
            raise GetSSMException(
                f"Get SSM Exception when retrieving parameter : '{path}'."
            ) from err
