import json
import logging
from typing import Union, AnyStr, Iterable

import boto3

from harvester.common.exceptions import S3UploadException

logger = logging.getLogger(__name__)


class S3Client:
    def __init__(self, region_name="eu-west-1"):
        self.client = boto3.client("s3", region_name=region_name)

    def write_file_to_s3(
        self, content: Union[AnyStr, list[AnyStr]], bucket: str, key: str
    ):
        try:
            logger.debug(f"Upload s3://{bucket}/{key}...")
            self.client.put_object(Body=content, Bucket=bucket, Key=key)
            logger.info(f"s3://{bucket}/{key} uploaded")
        except Exception as err:
            raise S3UploadException(
                f"Failed to upload S3 object 's3://{bucket}/{key}'"
            ) from err

    def get_file_iterator(self, bucket: str, prefix: str) -> Iterable:
        paginator = self.client.get_paginator("list_objects_v2")
        response_iterator = paginator.paginate(Bucket=bucket, Prefix=prefix)
        return response_iterator

    def get_object_as_dict(self, bucket: str, key: str) -> dict:
        response = self.client.get_object(Bucket=bucket, Key=key)
        try:
            return json.loads(response["Body"].read())
        except KeyError:
            logger.error(f"Object {key} not found in bucket {bucket}")
            return {}
