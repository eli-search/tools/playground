import logging

import boto3

from harvester.common.exceptions import GetSecretException


logger = logging.getLogger(__name__)


class SecretsManagerClient:
    def __init__(self, region_name="eu-west-1"):
        self.client = boto3.client("secretsmanager", region_name=region_name)

    def get_secret(self, secret_id: str) -> dict:
        try:
            logger.debug(
                f"Get secret value for secret_id: {secret_id} from Secrets Manager"
            )
            response = self.client.get_secret_value(SecretId=secret_id)
            return response
        except Exception as err:
            raise GetSecretException(
                f"Failed to retrieve secret {secret_id} from Secrets Manager"
            ) from err
