import logging
import os

from aws.neptune_client import NeptuneClient
from aws.opensearch_client import OpensearchClient
from aws.ssm_client import SSMClient
from indexer.indexer_config import IndexerConfig

logger = logging.getLogger(__name__)


environment = os.environ["ENV"]
SSM_OPENSEARCH_INDEX = f"/elisearch/{environment}/opensearch/index_name"


class Indexer:
    def __init__(self, input_data: dict):
        self.input_data = input_data
        self.indexer_config = IndexerConfig(self.input_data)

    def index(self):
        logger.info(f"Start indexing with input data: {self.input_data}")
        if (
            self.indexer_config.local_process
            or not self.indexer_config.indexing_enabled
        ):
            logger.warning("Indexing phase disabled.")
            return

        working_bucket = self.indexer_config.job_config.output_working_bucket

        logger.info(f"Start indexing data from {working_bucket} in Neptune")
        ttl_prefix = f"{self.indexer_config.execution_id}/ttl/"
        NeptuneClient().start_loader_job(working_bucket, ttl_prefix)
        logger.info("Neptune indexing done")

        logger.info(f"Start indexing data from {working_bucket} in Opensearch")
        jsonld_prefix = f"{self.indexer_config.execution_id}/jsonld/"
        index_name = SSMClient().get_ssm_parameter(SSM_OPENSEARCH_INDEX)
        OpensearchClient().bulk_index(
            index_name, working_bucket, jsonld_prefix, "jsonld"
        )
        logger.info("Opensearch indexing done")
