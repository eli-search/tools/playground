import logging

from app_config import AppConfig

logger = logging.getLogger(__name__)


class IndexerConfig(AppConfig):
    def __init__(self, input_data: dict):
        super().__init__(input_data)
        logger.info("Start loading the indexer configuration")
        self.indexing_enabled = input_data["enable_indexing"]
        logger.info("Indexer configuration loaded successfully")

    indexing_enabled: bool
